enchant();
window.onload = function() {
  game = new Core(320,320);
  game.preload('./img/matsuzaki1.jpg','./img/DVXH4yPUQAEyVEx2.jpg','http://jsrun.it/assets/Y/M/L/5/YML5t.png','http://jsrun.it/assets/G/X/w/W/GXwWD.png','http://jsrun.it/assets/O/t/P/K/OtPKv.png','http://jsrun.it/assets/0/i/r/B/0irBJ.png');
  game.onload = function() {
    gameScene = game.rootScene;
    enemies = [];
    score = 0;
    var sky = [];
    for (var i=0; i<2; i++) {
      sky[i] = new Sprite(320,320);
      sky[i].image = game.assets['./img/matsuzaki1.jpg'];
      sky[i].moveTo(0,-320*i);
      gameScene.addChild(sky[i]);
    }
    player = new Player(135,200);
    gameScene.addChild(player);
    var scoreTxst = new Label();
    scoreTxst.color = 'white';
    gameScene.addChild(scoreTxst);
    gameScene.onenterframe = function() {
      scoreTxst.text = "Score：" + score;
      for (var i=0; i<2; i++) {
        sky[i].y += 5;
        if (sky[i].y === 320) sky[i].y = -320;
      }
      if (game.frame % 10 === 0) {
        enemies[game.frame] = new Enemy(50,50);
        gameScene.addChild(enemies[game.frame]);
        enemies[game.frame].extence = true;
      }
    };
    game.gameoverScene = function() {
      var scene = new Scene();
      var label = new Label("GAME OVER");
      label.moveTo(120,160);
      scene.addChild(label);
      return scene;
    };
  };
  game.start();
};
var Player = Class.create(Sprite, {
	initialize: function() {
		Sprite.call(this,50,50);
    this.moveTo(135,200);
		this.image = game.assets['http://jsrun.it/assets/Y/M/L/5/YML5t.png'];
	}, ontouchstart: function(e){
		px = e.x - this.x; py = e.y - this.y;
    var shot = new Shot(this.x+24,this.y,0);
    gameScene.addChild(shot);
	}, ontouchmove: function(e) {
    this.x = e.x - px; this.y = e.y - py;
    if (this.x > 270) this.x = 270;
    else if (this.x < 0) this.x = 0;
    else if (this.y > 270) this.y = 270;
    else if (this.y < 0) this.y = 0;
  }
});
var Enemy = Class.create(Sprite, {
	initialize: function() {
    var rnd = Math.floor(Math.random() * 470);
		Sprite.call(this,50,50);
    this.moveTo(rnd,-50);
		this.image = game.assets['./img/DVXH4yPUQAEyVEx2.jpg'];
	}, onenterframe: function() {
    this.y += 3;
    if (this.y > 330) gameScene.removeChild(this);
    if (this.within(player, 15)) game.pushScene(game.gameoverScene());
    if (this.age % 25 === 0) {
      var shot = new Shot(this.x+24,this.y+50,1);
      gameScene.addChild(shot);
    }
  }
});
var Shot = Class.create(Sprite, {
	initialize: function(x,y,type) {
		Sprite.call(this,2,5);
    this.moveTo(x,y);
		this.image = game.assets['http://jsrun.it/assets/O/t/P/K/OtPKv.png'];
    this.type = type;
	}, onenterframe: function() {
    if (this.type === 0) {
      this.y -= 5;
      for(var i in enemies){
        if (!enemies[i].extence) continue;
        if (enemies[i].within(this,20)) {
          gameScene.removeChild(this);
          gameScene.removeChild(enemies[i]);
          enemies[i].extence = false;
          score+=100;
        }
      }
    } else if (this.type === 1) {
      this.y += 5;
      if (this.within(player, 15)) game.pushScene(game.gameoverScene());
    }
    if (this.y < -10 || this.y > 330) gameScene.removeChild(this);
  }
});
