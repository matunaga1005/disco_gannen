/**
 * @fileOverview
 * nineleap.enchant.js
 * @version 0.3.4 (2013/04/03)
 * @requires enchant.js v0.6.3 or later
 *
 * @description
 * enchant.js extension for 9leap.net
 * 9leap.net 蜷代￠縺ｮ enchant.js 諡｡蠑ｵ繝励Λ繧ｰ繧､繝ｳ縲�
 * core.end 縺ｮ蠑墓焚縺ｫ繧ｹ繧ｳ繧｢縺ｨ邨先棡縺ｮ譁�ｭ怜�繧呈ｸ｡縺吶％縺ｨ縺ｧ縲√Λ繝ｳ繧ｭ繝ｳ繧ｰ縺ｫ逋ｻ骭ｲ縺ｧ縺阪ｋ縲�
 * (9leap縺ｫ繧｢繝��繝ｭ繝ｼ繝峨＠縺溷ｾ後�縺ｿ繝ｩ繝ｳ繧ｭ繝ｳ繧ｰ逕ｻ髱｢縺ｫ繧ｸ繝｣繝ｳ繝励☆繧�)
 *
 * @usage
 *
 * var core = new Core(320, 320);
 *
 * core.onload = function(){
 * // executed after player pushed "START"
 * // ...
 * if(some.condition)core.end(score, result);
 * };
 *
 * core.start();
 */

(function() {

    /**
     * @type {Object}
     */
    enchant.nineleap = { assets: ['start.png', 'end.png'] };

    /**
     * @scope enchant.nineleap.Core.prototype
     */
    enchant.nineleap.Core = enchant.Class.create(enchant.Core, {
        /**
         * start, gameover 縺ｮ逕ｻ蜒上ｒ陦ｨ遉ｺ縺励�
         * 譛蠕後↓繧ｹ繧ｳ繧｢繧帝∽ｿ｡縺吶ｋ繧医≧縺ｫ諡｡蠑ｵ縺輔ｌ縺� Core 繧ｯ繝ｩ繧ｹ
         * @param width
         * @param height
         * @constructs
         */
        initialize: function(width, height) {
            enchant.Core.call(this, width, height);
            this.addEventListener('load', function() {
                var core = this;
                this.startScene = new enchant.nineleap.SplashScene();
                this.startScene.image = this.assets['start.png'];
                this.startScene.addEventListener('touchend', function() {
                    if (core.started === false) {
                        if (core.onstart != null) {
                            core.onstart();
                        }
                        core.started = true;
                        coreStart = true;   // deprecated
                    }
                    if (core.currentScene === this) {
                        core.popScene();
                    }
                    this.removeEventListener('touchend', arguments.callee);
                });
                this.addEventListener('keydown', function() {
                    if (this.started === false) {
                        if (this.onstart != null) {
                            this.onstart();
                        }
                        this.started = true;
                    }
                    if (core.currentScene === core.startScene) {
                        core.popScene();
                    }
                    this.removeEventListener('keydown', arguments.callee);
                });
                this.pushScene(this.startScene);

                this.endScene = new enchant.nineleap.SplashScene();
                this.endScene.image = this.assets['end.png'];
                this.removeEventListener('load', arguments.callee);
            });
            this.scoreQueue = false;
            this.started = false;
            coreStart = false; // deprecated
        },

        _requestPreload: function() {
            var o = {};
            var loaded = 0,
                len = 0,
                loadFunc = function() {
                    var e = new enchant.Event('progress');
                    e.loaded = ++loaded;
                    e.total = len;
                    enchant.Core.instance.loadingScene.dispatchEvent(e);
                };
            this._assets
                .concat(this._twitterAssets || [])
                .concat(this._netpriceData || [])
                .concat(this._memoryAssets || [])
                .reverse()
                .forEach(function(asset) {
                    var src, name;
                    if (asset instanceof Array) {
                        src = asset[0];
                        name = asset[1];
                    } else {
                        src = name = asset;
                    }
                    if (!o[name]) {
                        o[name] = this.load(src, name, loadFunc);
                        len++;
                    }
                }, this);

            this.pushScene(this.loadingScene);
            return enchant.Deferred.parallel(o);
        },

        end: function(score, result, img) {
            if (img !== undefined) {
                this.endScene.image = img;
            }
            this.pushScene(this.endScene);
            if (location.hostname === 'r.jsgames.jp') {
                var submit = function() {
                    var id = location.pathname.match(/^\/games\/(\d+)/)[1];
                    location.replace([
                        'http://9leap.net/games/', id, '/result',
                        '?score=', encodeURIComponent(score),
                        '&result=', encodeURIComponent(result)
                    ].join(''));
                };
                this.endScene.addEventListener('touchend', submit);
                window.setTimeout(submit, 3000);
            }
            enchant.Core.instance.end = function() {
            };
        }
    });

    /**
     * @scope enchant.nineleap.SplashScene.prototype
     */
    enchant.nineleap.SplashScene = enchant.Class.create(enchant.Scene, {
        /**
         * @extends enchant.Scene
         * @constructs
         * 繧ｹ繝励Λ繝�す繝･逕ｻ蜒上ｒ陦ｨ遉ｺ縺吶ｋ繧ｷ繝ｼ繝ｳ縲�
         */
        initialize: function() {
            enchant.Scene.call(this);
        },

        /**
         * 荳ｭ螟ｮ縺ｫ陦ｨ遉ｺ縺吶ｋ逕ｻ蜒�
         * @type {enchant.Surface}
         */
        image: {
            get: function() {
                return this._image;
            },
            set: function(image) {
                this._image = image;

                // discard all child nodes
                while (this.firstChild) {
                    this.removeChild(this.firstChild);
                }

                // generate an Sprite object and put it on center
                var sprite = new enchant.Sprite(image.width, image.height);
                sprite.image = image;
                sprite.x = (this.width - image.width) / 2;
                sprite.y = (this.height - image.height) / 2;
                this.addChild(sprite);
            }
        }
    });

})();
